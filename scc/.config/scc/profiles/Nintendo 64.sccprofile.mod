{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.BTN_GAMEPAD)"
        }, 
        "BACK": {
            "action": "button(Keys.BTN_SELECT)"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "LB": {
            "action": "button(Keys.BTN_TL)"
        }, 
        "LGRIP": {
            "action": "button(Keys.BTN_GAMEPAD)"
        }, 
        "RB": {
            "action": "button(Keys.BTN_TR)"
        }, 
        "RGRIP": {
            "action": "button(Keys.BTN_NORTH)"
        }, 
        "RPAD": {
            "action": "button(Keys.BTN_THUMBR)"
        }, 
        "START": {
            "action": "button(Keys.BTN_START)"
        }, 
        "STICKPRESS": {
            "action": "button(Keys.BTN_THUMBL)"
        }, 
        "X": {
            "action": "button(Keys.BTN_NORTH)"
        }
    }, 
    "cpad": {}, 
    "gyro": {
        "action": "cemuhook()"
    }, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "XY(axis(Axes.ABS_X), raxis(Axes.ABS_Y))"
    }, 
    "pad_right": {
        "action": "feedback(RIGHT, 256, dpad(10, axis(Axes.ABS_RX, 0, -32767), axis(Axes.ABS_RX, 0, 32767), axis(Axes.ABS_RY, 0, -32767), axis(Axes.ABS_RY, 0, 32767)))"
    }, 
    "stick": {
        "action": "XY(axis(Axes.ABS_X), raxis(Axes.ABS_Y))"
    }, 
    "trigger_left": {
        "action": "axis(Axes.ABS_RZ)"
    }, 
    "trigger_right": {
        "action": "axis(Axes.ABS_Z)"
    }, 
    "version": 1.4
}