// ==UserScript==
// @name          Dark Reader (Unofficial)
// @icon          https://darkreader.org/images/darkreader-icon-256x256.png
// @namespace     DarkReader
// @description	  Inverts the brightness of pages to reduce eye strain
// @version       4.7.15
// @author        https://github.com/darkreader/darkreader#contributors
// @homepageURL   https://darkreader.org/ | https://github.com/darkreader/darkreader
// @run-at        document-end
// @grant         none
// @include       http*
// @exclude        /^https?://.*amazon\.com/?/
// @exclude        /^https?://.*youtube\.com/?/
// @exclude        /^https?://.*google\.com/?/
// @exclude        /^https?://.*urbanpizzacompany\.com.au/?/
// @exclude        /^https?://.*gumtree\.com.au/?/
// @exclude        /^https?://.*odysee\.com/?/
// @exclude        /^https?://.*github\.com/?/
// @exclude        /^https?://.*heritage\.com/?/
// @exclude        /^https?://.*booking\.com/?/
// @exclude        /^https?://.*robertsspaceindustries\.com/?/
// @exclude        /^https://.*rebelsport\.com.au/?/
// @exclude        /^https://.*flightcentre\.com.au/?/
// @exclude        /^https://.*xe\.com/?/
// @exclude        /^https://.*jbhifi\.com.au/?/
// @require       https://cdn.jsdelivr.net/npm/darkreader/darkreader.min.js
// @noframes
// ==/UserScript==

DarkReader.enable({
	brightness: 80,
	contrast: 80,
	sepia: 0
});
