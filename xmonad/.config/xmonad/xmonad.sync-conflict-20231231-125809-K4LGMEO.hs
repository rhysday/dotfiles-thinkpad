    -- Base
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W
import Graphics.X11.ExtraTypes.XF86

    -- Actions
import XMonad.Actions.CopyWindow (copyToAll, killAllOtherCopies, kill1)
import XMonad.Actions.CycleWS (moveTo, shiftTo, WSType(..), nextScreen, prevScreen, nextWS, prevWS, toggleWS)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import qualified XMonad.Actions.TreeSelect as TS
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S
import XMonad.Actions.GroupNavigation

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docks, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat, doRectFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory
--import XMonad.Hooks.WindowSwallowing

    -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.TrackFloating

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders (smartBorders, noBorders)
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

    -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Input
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Man
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell
import XMonad.Prompt.Ssh
import XMonad.Prompt.Unicode
import XMonad.Prompt.XMonad
import Control.Arrow (first)

   -- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

myFont :: String
myFont = "xft:RobotoFlex:size=10:antialias=true:hinting=true"

myEmojiFont :: String
myEmojiFont = "xft:JoyPixels:regular:size=9:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask       -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "alacritty"   -- Sets default terminal

myBrowser :: String
myBrowser = "qutebrowser-wrap "               -- Sets qutebrowser as browser for tree select
-- myBrowser = myTerminal ++ " -e lynx " -- Sets lynx as browser for tree select

myEditor :: String
myEditor = "emacsclient -c -a emacs "  -- Sets emacs as editor for tree select
-- myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor for tree select

myBorderWidth :: Dimension
myBorderWidth = 2          -- Sets border width for windows

myNormColor :: String
myNormColor = "#282c34"  -- Border color of normal windows

myFocusColor :: String
myFocusColor = "#46d9ff"  -- Border color of focused windows

altMask :: KeyMask
altMask = mod1Mask         -- Setting this for use in xprompts

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook :: X ()
myStartupHook = do
         spawnOnce "feh --bg-fill --randomize --no-fehbg ~/.local/bin/wallpapers/* &"

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
-- REPLACE $ mySpacing in layouts below with this one: mySpacing'
-- mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
-- mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall     = renamed [Replace "tall"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ limitWindows 12
           $ mySpacing 3
           $ ResizableTall 1 (3/100) (1/2) []
--magnify  = renamed [Replace "magnify"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ magnifier
--           $ limitWindows 12
--           $ mySpacing 8
--           $ ResizableTall 1 (3/100) (1/2) []
--monocle  = renamed [Replace "monocle"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ limitWindows 20 Full
--floats   = renamed [Replace "floats"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ limitWindows 12
           $ mkToggle (single MIRROR)
           $ mySpacing 3
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
          -- $ subLayout [] (smartBorders Simplest)
           $ mySpacing 3
           $ spiral (6/7)
--threeCol = renamed [Replace "threeCol"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ limitWindows 7
--           $ ThreeCol 1 (3/100) (1/2)
--threeRow = renamed [Replace "threeRow"]
--           $ windowNavigation
--           $ addTabs shrinkText myTabTheme
--           $ limitWindows 7
--           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
myLayoutHook = avoidStruts $ mouseResize $ windowArrange
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     tall
                                 ||| spirals
                                 -- ||| magnify
                                 -- ||| noBorders monocle
                                 -- ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 -- ||| threeCol
                                 -- ||| threeRow

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x28,0x2c,0x34) -- lowest inactive bg
                  (0x28,0x2c,0x34) -- highest inactive bg
                  (0xc7,0x92,0xea) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 6
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 40
                   , gs_cellwidth    = 200
                   , gs_cellpadding  = 6
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   }

myAppGrid = [ ("Telegram", "telegram-wrap")
                 , ("Discord", "discord-wrap")
                 , ("Krita", "krita")
                 , ("Inkscape", "inkscape")
                 , ("Librewolf", "librewolf-wrap")
                 , ("Gimp", "gimp-wrap")
                 , ("Nintendo64", "nintendo64-wrap")
                 , ("Ardour", "ardour-wrap")
                 , ("Nicotine", "nicotine")
                 , ("LBRY", "lbry-wrap")
                 ]

myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "ncmpcpp" spawnNCMPCPP findNCMPCPP manageNCMPCPP
                , NS "htop" spawnHtop findHtop manageHtop
                , NS "transmission" spawnTrans findTrans manageTrans
                , NS "ncpamixer" spawnNCPA findNCPA manageNCPA
                ]

 where
    spawnTerm  = myTerminal ++ " --class scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.8
                 t = 0.9 -h
                 l = 0.9 -w
    spawnNCMPCPP  = myTerminal ++ " --class ncmpcpp -e ncmpcpp"
    findNCMPCPP   = resource =? "ncmpcpp"
    manageNCMPCPP = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.8
                 t = 0.9 -h
                 l = 0.9 -w
    spawnHtop  = myTerminal ++ " --class htop -e htop"
    findHtop   = resource =? "htop"
    manageHtop = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.8
                 t = 0.9 -h
                 l = 0.9 -w
    spawnTrans  = myTerminal ++ " --class transmission -e tremc -X"
    findTrans   = resource =? "transmission"
    manageTrans = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.8
                 t = 0.9 -h
                 l = 0.9 -w
    spawnNCPA  = myTerminal ++ " --class ncpamixer -e ncpamixer"
    findNCPA   = resource =? "ncpamixer"
    manageNCPA = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.8
                 t = 0.9 -h
                 l = 0.9 -w

dtXPConfig :: XPConfig
dtXPConfig = def
      { font                = myFont
      , bgColor             = "#282c34"
      , fgColor             = "#bbc2cf"
      , bgHLight            = "#c792ea"
      , fgHLight            = "#000000"
      , borderColor         = "#535974"
      , promptBorderWidth   = 0
      , promptKeymap        = dtXPKeymap
      , position            = Top
      -- , position            = CenteredAt { xpCenterY = 0.3, xpWidth = 0.3 }
      , height              = 23
      , historySize         = 0
      , historyFilter       = id
      , defaultText         = []
      , autoComplete        = Just 100000  -- set Just 100000 for .1 sec
      , showCompletionOnTab = False
      -- , searchPredicate     = isPrefixOf
      , searchPredicate     = fuzzyMatch
      , defaultPrompter     = id $ map toUpper  -- change prompt to UPPER
      -- , defaultPrompter     = unwords . map reverse . words  -- reverse the prompt
      -- , defaultPrompter     = drop 5 .id (++ "XXXX: ")  -- drop first 5 chars of prompt and add XXXX:
      , alwaysHighlight     = True
      , maxComplRows        = Nothing      -- set to 'Just 5' for 5 rows
      }

-- The same config above minus the autocomplete feature which is annoying
-- on certain Xprompts, like the search engine prompts.
dtXPConfig' :: XPConfig
dtXPConfig' = dtXPConfig
      { autoComplete        = Nothing
      }

emojiXPConfig :: XPConfig
emojiXPConfig = dtXPConfig
      { font             = myEmojiFont
      }



calcPrompt c ans =
    inputPrompt c (trim ans) ?+ \input ->
        liftIO(runProcessWithInput "qalc" [input] "") >>= calcPrompt c
    where
        trim  = f . f
            where f = reverse . dropWhile isSpace



dtXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
dtXPKeymap = M.fromList $
     map (first $ (,) controlMask)      -- control + <key>
     [ (xK_z, killBefore)               -- kill line backwards
     , (xK_k, killAfter)                -- kill line forwards
     , (xK_a, startOfLine)              -- move to the beginning of the line
     , (xK_e, endOfLine)                -- move to the end of the line
     , (xK_m, deleteString Next)        -- delete a character foward
     , (xK_b, moveCursor Prev)          -- move cursor forward
     , (xK_f, moveCursor Next)          -- move cursor backward
     , (xK_BackSpace, killWord Prev)    -- kill the previous word
     , (xK_y, pasteString)              -- paste a string
     , (xK_g, quit)                     -- quit out of prompt
     , (xK_bracketleft, quit)
     ]
     ++
     map (first $ (,) altMask)          -- meta key + <key>
     [ (xK_BackSpace, killWord Prev)    -- kill the prev word
     , (xK_f, moveWord Next)            -- move a word forward
     , (xK_b, moveWord Prev)            -- move a word backward
     , (xK_d, killWord Next)            -- kill the next word
     , (xK_n, moveHistory W.focusUp')   -- move up thru history
     , (xK_p, moveHistory W.focusDown') -- move down thru history
     ]
     ++
     map (first $ (,) 0) -- <key>
     [ (xK_Return, setSuccess True >> setDone True)
     , (xK_KP_Enter, setSuccess True >> setDone True)
     , (xK_BackSpace, deleteString Prev)
     , (xK_Delete, deleteString Next)
     , (xK_Left, moveCursor Prev)
     , (xK_Right, moveCursor Next)
     , (xK_Home, startOfLine)
     , (xK_End, endOfLine)
     , (xK_Down, moveHistory W.focusUp')
     , (xK_Up, moveHistory W.focusDown')
     , (xK_Escape, quit)
     ]

archwiki, ebay, news, reddit, urban, archive, odysee, flickr, imdb :: S.SearchEngine

archwiki   = S.searchEngine "Archwiki"      "https://wiki.archlinux.org/index.php?search="
aur        = S.searchEngine "AUR"           "https://aur.archlinux.org/packages/?O=0&K="
archive    = S.searchEngine "Archive"       "https://www.archive.org/search.php?query="
amazon     = S.searchEngine "Amazon"        "https://www.amazon.com.au/s?k="
ebay       = S.searchEngine "Ebay"          "https://www.ebay.com/sch/i.html?_nkw="
etsy       = S.searchEngine "Etsy"          "https://www.etsy.com/uk/search?q="
news       = S.searchEngine "News"          "https://news.google.com/search?q="
reddit     = S.searchEngine "Reddit"        "https://www.reddit.com/search/?q="
urban      = S.searchEngine "Urban"         "https://www.urbandictionary.com/define.php?term="
brave      = S.searchEngine "Brave Search"  "https://search.brave.com/search?q="
-- searx      = S.searchEngine "Searx"      "https://searx.info/?q="
thesaurus  = S.searchEngine "Thesaurus"     "https://www.thesaurus.com/browse/"
alamy      = S.searchEngine "Alarmy"        "https://www.alamy.com/search.html?CreativeOn=1&adv=1&ag=0&all=1&creative=&et=0x000000000000000000000&vp=0&loc=0&qt="
artstation = S.searchEngine "Artstation"    "https://www.artstation.com/search?q="
flickr     = S.searchEngine "Flickr"        "https://flickr.com/search/?q="
pixabay    = S.searchEngine "Pixabay"       "https://pixabay.com/images/search/"
pinterest  = S.searchEngine "Pinterest"     "https://www.pinterest.com.au/search/pins/?q="
images     = S.searchEngine "Brave Images"  "https://search.brave.com/images?q="
odysee     = S.searchEngine "Odysee"        "https://odysee.com/$/search?q="
imdb       = S.searchEngine "IMDB"          "https://www.imdb.com/find?q="

-- This is the list of search engines that I want to use. Some are from
-- XMonad.Actions.Search, and some are the ones that I added above.
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a",   archwiki)
             , ("S-a", aur)
             , ("g",   S.google)
             , ("h",   S.hoogle)
             , ("m",   S.maps)
             , ("S-m", imdb)
             , ("i",   images)
             , ("S-i", alamy)
             , ("f",   flickr)
             , ("S-f", artstation)
             , ("p",   pixabay)
             , ("S-p", pinterest)
             --, ("y", S.youtube)
             , ("n",   news)
             , ("r",   archive)
             , ("S-r", reddit)
             , ("s",   S.stackage)
             , ("S-w", S.wikipedia)
             , ("w",   S.wiktionary)
             , ("t",   thesaurus)
             , ("v",   S.vocabulary)
             , ("u",   urban)
             , ("b",   S.wayback)
             , ("e",   ebay)
             , ("S-e", etsy)
             , ("z",   amazon)
             , ("x",   brave)
             , ("o",   odysee)
             ]

myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:RobotoFlex:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#bfbfbf"
    }

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     [ title =? "Mozilla Firefox"                  --> doShift ( myWorkspaces !! 1 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , className =? "TelegramDesktop"             --> doShift ( myWorkspaces !! 1 )
     , className =? "discord"                     --> doShift ( myWorkspaces !! 1 )
     , className =? "REAPER"                      --> doShift ( myWorkspaces !! 2 )
     , title =? "REAPER (initializing)"                    --> doFloat
     , className =? "Ardour"                      --> doShift ( myWorkspaces !! 2 )
--     , className =? "Ardour"                               --> doFloat
--     , title =? "Ardour"                          --> doShift ( myWorkspaces !! 2 )
--     , title =? "Ardour"                                   --> doFloat
--     , title =? "Pre-Release Warning"             --> doShift ( myWorkspaces !! 2 )
--     , title =? "Pre-Release Warning"                      --> doFloat
--     , title =? "Session Setup"                   --> doShift ( myWorkspaces !! 2 )
--     , title =? "Session Setup"                            --> doFloat
--     , className =? "Gimp-2.99"                         --> doShift ( myWorkspaces !! 2 )
     , className =? "Gimp-2.99"                            --> doFloat
     , className =? "resolve"                      --> doShift ( myWorkspaces !! 3 )
     , className =? "Blender"                      --> doShift ( myWorkspaces !! 3 )
--     , className =? "Blender"                              --> doCenterFloat
     --, title =? "Blender Preferences"                      --> doCenterFloat
     --, title =? "Blender Render"                           --> doFloat
     , className =? "Fusion"                       --> doShift ( myWorkspaces !! 4 )
     , className =? "Natron"                       --> doShift ( myWorkspaces !! 4 )
     , className =? "Natron"                               --> doFloat
--     , className =? "krita"                        --> doShift ( myWorkspaces !! 5 )
--     , className =? "Inkscape"                     --> doShift ( myWorkspaces !! 5 )
     , className =? "Houdini FX"                   --> doShift ( myWorkspaces !! 5 )
     , title =? "Houdini FX"                               --> doFloat
     , title =? "Percent completion application"           --> doFloat
     , title =? "File Operation Progress"                  --> doFloat
     , className =? "Mplay"                                --> doFloat
     , className =? "UE4Editor"                    --> doShift ( myWorkspaces !! 5 )
     , className =? "UE4Editor"                            --> doCenterFloat
     , className =? "LBRY"                         --> doShift ( myWorkspaces !! 6 )
     , className =? "arma3-unix-launcher"          --> doShift ( myWorkspaces !! 6 )
     , title =? "TeamSpeak 3"             --> doShift ( myWorkspaces !! 6 )
     , className =? "Steam"                        --> doShift ( myWorkspaces !! 6 )
     , className =? "steam_app_107410"             --> doShift ( myWorkspaces !! 7 )
     , className =? "steam_app_16900"              --> doShift ( myWorkspaces !! 7 )
     , className =? "7DaysToDie.x86_64"            --> doShift ( myWorkspaces !! 7 )
     , className =? "m64py"                        --> doShift ( myWorkspaces !! 7 )
     , className =? "SC Controller"                --> doShift ( myWorkspaces !! 8 )
     , className =? "Non-Mixer"                    --> doShift ( myWorkspaces !! 8 )
     , className =? "Carla2"                       --> doShift ( myWorkspaces !! 8 )
     , className =? "Virt-manager"                 --> doShift ( myWorkspaces !! 8 )
     , className =? "Pinentry-gtk-2"                    --> doRectFloat(W.RationalRect 0 0 0.20 0.15) --x y w h
     , className =? "KeePassXC"                            --> doFloat
     , className =? "wootility"                            --> doFloat
     , className =? "stalonetray"                          --> doFloat
     ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = return ()

myKeys :: [(String, X ())]
myKeys  =
    -- Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")                                                                        -- Recompiles xmonad
        , ("M-S-r", spawn "xmonad --restart")                                                                          -- Restarts xmonad
        , ("M-M1-S-q", io exitSuccess)                                                                                    -- Quits xmonad
        , ("M-S-v", spawn "{ killall picom || setsid picom & }")                                                       -- Restart Picom
        , ("M-a",   spawn  (myTerminal ++ " -e feh --bg-fill --randomize --no-fehbg ~/.local/bin/wallpapers/* & "))    -- Random Wallpaper

    -- Run Prompt
        , ("M-S-<Return>", spawn "dmenu_run") -- Dmenu
        , ("M-<Print>",   spawn  "dmenurecord & ")                                                                     -- Video record
        , ("M-M1-<Print>",   spawn  "dmenurecord kill & ")                                                             -- Video stop record
        , ("M-S-p", spawn "rofi-pass") -- Pass
        , ("<XF86PowerOff>", spawn "[ \"$(printf \"No\\nYes\" | dmenu -i -nb darkred -sb red -sf white -nf gray -p \"Suspend computer?\")\" = Yes ] && systemctl suspend")
        , ("M-`",   spawn  "dmenuunicode & ")                                                                          -- Emoticon dmenu list

    -- Other Prompts
        , ("M-p c", calcPrompt dtXPConfig' "qalc") -- calcPrompt
        , ("M-p m", manPrompt dtXPConfig)          -- manPrompt
        , ("M-p p", passPrompt dtXPConfig)         -- passPrompt
        , ("M-p g", passGeneratePrompt dtXPConfig) -- passGeneratePrompt
        , ("M-p r", passRemovePrompt dtXPConfig)   -- passRemovePrompt
        , ("M-p s", sshPrompt dtXPConfig)          -- sshPrompt
        , ("M-p x", xmonadPrompt dtXPConfig)       -- xmonadPrompt

    -- Useful programs to have a keybinding for launch
        , ("M-<Return>", spawn (myTerminal))
        , ("M-M1-b", spawn (myBrowser ))
        , ("M-M1-o", spawn (myBrowser ++ " --basedir ~/.local/data/odysee/ --config-py ~/.config/qutebrowser/config_other.py 'https://www.odysee.com'"))
        , ("M-M1-r", spawn ("thunar"))
        , ("M-M1-f", spawn ("freetube-wrap"))
        , ("M-s y", spawn (myTerminal ++ " -e ytfzf -t"))                                                            -- YouTube Search Using YTFZF
        , ("M-M1-t", spawn "{ tray & }")                                                                               -- Opens Trayer
        , ("M-M1-S-t", spawn "{ killall -9 trayer & }")                                                                -- Closes Trayer
        , ("M-M1-h", spawn (myTerminal ++ " -e htop"))
        , ("M-<F1>",   spawn  "zathura $XDG_DATA_HOME/larbs/blender_2-93_hotkey_sheet_v8_color.pdf & ")                -- Blender Cheatsheet
        , ("M-<F2>",   spawn  "zathura $XDG_DATA_HOME/larbs/CheatSheet_reallyfastcut_0116.pdf & ")                     -- Blender VSE Cheatsheet
        , ("M1-m", spawn (myTerminal ++ " -e cava"))
        , ("M-<F5>",   spawn  "dmenuumount & ")                                                                        -- Unmount Drives
        , ("M-<F6>",   spawn  "dmenumount & ")                                                                         -- Mount Drives
        , ("M-<F12>",  spawn (myTerminal ++ " -e ytmpv"))                                                              -- Watch YouTube Links
        , ("<Print>",   spawn  "flameshot gui & ")                                                                     -- Screen Capture
        , ("M-<F10>",  spawn  (myTerminal ++ " -e sudo win10 &"))                                                      -- Windows Virtual Machine

    -- Kill windows
        , ("M-q", kill1)     -- Kill the currently focused client
        , ("M-S-a", killAll)   -- Kill all windows on current workspace

    -- Workspaces
        -- , ("M-.", nextScreen)  -- Switch focus to next monitor
        -- , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-<Tab>", toggleWS)
        , ("M-,", nextMatch History (return True))
        , ("M-S-.", shiftTo Next nonNSP >> moveTo Next nonNSP)  -- Shifts focused window to next ws
        , ("M-S-,", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws
        , ("M-<Left>",  prevWS)
        , ("M-<Right>", nextWS)

    -- Floating windows
        , ("M-S-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
        , ("M-t", withFocused $ windows . W.sink)    -- Push floating window back to tile
        , ("M-S-t", sinkAll)                         -- Push ALL floating windows to tile

    -- Increase/decrease spacing (gaps)
        , ("M-M1-g",   decScreenWindowSpacing 3)     -- Decrease all spacing
        , ("M-M1-S-g", incScreenWindowSpacing 3)     -- Increase all spacing
        , ("M-d",   decWindowSpacing 3)           -- Decrease window spacing
        , ("M-i",   incWindowSpacing 3)           -- Increase window spacing
        , ("M-S-d", decScreenSpacing 3)         -- Decrease screen spacing
        , ("M-S-i", incScreenSpacing 3)         -- Increase screen spacing

    -- Grid Select (CTR-g followed by a key)
        , ("M-g", spawnSelected' myAppGrid)                 -- grid select favorite apps
        , ("M-t", goToSelected $ mygridConfig myColorizer)  -- goto selected window
        , ("M-b", bringSelected $ mygridConfig myColorizer) -- bring selected window

    -- Tree Select
        --, ("C-t t", treeselectAction tsDefaultConfig)

    -- Windows navigation
        , ("M-m", windows W.focusMaster)  -- Move focus to the master window
        , ("M-j", windows W.focusDown)    -- Move focus to the next window
        , ("M-k", windows W.focusUp)      -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
        , ("M-<Space>", promote)          -- Moves focused window to master, others maintain order
        , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
        , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack
        , ("M-S-s", windows copyToAll)
        , ("M-C-s", killAllOtherCopies)

    -- Layouts
        , ("M-y", sendMessage (NextLayout) >> (sinkAll))       -- Switch to next layout
        , ("M-C-M1-<Up>", sendMessage Arrange)
        , ("M-C-M1-<Down>", sendMessage DeArrange)
        , ("M-f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        , ("M-M1-n", sendMessage $ MT.Toggle NOBORDERS)  -- Toggles noborder
        , ("M-S-<Space>", sendMessage ToggleStruts)     -- Toggles struts

    -- Increase/decrease windows in the master pane or the stack
        , ("M-S-<Up>", sendMessage (IncMasterN 1))      -- Increase number of clients in master pane
        , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease number of clients in master pane
        , ("M-C-<Up>", increaseLimit)                   -- Increase number of windows
        , ("M-C-<Down>", decreaseLimit)                 -- Decrease number of windows

    -- Window resizing
        , ("M-h",    sendMessage Shrink)                 -- Shrink horiz window width
        , ("M-l",    sendMessage Expand)                 -- Expand horiz window width
        , ("M-M1-j", sendMessage MirrorShrink)           -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)           -- Exoand vert window width

    -- Sublayouts
    -- This is used to push windows to tabbed sublayouts, or pull them out of it.
        , ("M-C-h", sendMessage $ pullGroup L)
        , ("M-C-l", sendMessage $ pullGroup R)
        , ("M-C-k", sendMessage $ pullGroup U)
        , ("M-C-j", sendMessage $ pullGroup D)
        , ("M-C-m", withFocused (sendMessage . MergeAll))
        , ("M-C-u", withFocused (sendMessage . UnMerge))
        , ("M-C-/", withFocused (sendMessage . UnMergeAll))
        , ("M-C-.", onGroup W.focusUp')    -- Switch focus to next tab
        , ("M-C-,", onGroup W.focusDown')  -- Switch focus to prev tab

    -- Scratchpads
        , ("M-M1-c",        namedScratchpadAction myScratchPads "terminal")
        , ("M-M1-m",        namedScratchpadAction myScratchPads "ncmpcpp")
        , ("M-M1-p",        namedScratchpadAction myScratchPads "ncpamixer")
        , ("M-M1-x",        namedScratchpadAction myScratchPads "transmission")
        , ("M-M1-<Delete>", namedScratchpadAction myScratchPads "htop")

    -- Emacs (CTRL-e followed by a key)
        , ("M-e e", spawn "emacsclient -c -a 'emacs'")                            -- start emacs
        , ("M-e b", spawn "emacsclient -c -a 'emacs' --eval '(ibuffer)'")         -- list emacs buffers
        , ("M-e d", spawn "emacsclient -c -a 'emacs' --eval '(dired nil)'")       -- dired emacs file manager
        , ("M-e i", spawn "emacsclient -c -a 'emacs' --eval '(erc)'")             -- erc emacs irc client
        , ("M-e m",   spawn "emacs --eval '(mu4e)'")            -- mu4e emacs email client
        --, ("M-e m", spawn "emacsclient -c -a 'emacs' --eval '(mu4e)'")            -- mu4e emacs email client
        , ("M-e f", spawn "emacs --eval '(elfeed)'")          -- elfeed emacs rss client (OWN INSTANCE)
        , ("M-e s", spawn "emacsclient -c -a 'emacs' --eval '(eshell)'")          -- eshell within emacs
        , ("M-e a", spawn "emacsclient -c -a 'emacs' --eval '(org-agenda)'")      -- org-agenda menu within emacs
        , ("M-e n", spawn "emacsclient -c -a 'emacs' 'org/notes.gpg'")           -- My notes within emacs
        --, ("M-n", spawn "emacs 'org/notes.gpg'")            -- My notes within emacs
        , ("M-e g", spawn "emacsclient -c -a 'emacs' 'org/gtd.gpg'")              -- My GTD within emacs

 -- Multimedia Keys
        , ("<XF86AudioPlay>",         spawn ("mpc toggle"))
        , ("<XF86AudioPrev>",         spawn ("mpc prev"))
        , ("<XF86AudioNext>",         spawn ("mpc next"))
        , ("<XF86AudioMute>",         spawn ("pamixer -t"))
        , ("<XF86AudioMicMute>",      spawn ("pamixer --default-source -t"))
        , ("<XF86AudioLowerVolume>",  spawn ("pamixer -u -d 5"))
        , ("<XF86AudioRaiseVolume>",  spawn ("pamixer -u -i 5"))
        , ("<XF86MonBrightnessUp>",   spawn ("xbacklight -inc 5"))
        , ("<XF86MonBrightnessDown>", spawn ("xbacklight -dec 5"))
        ]
    -- Appending search engine prompts to keybindings list.
    -- Look at "search engines" section of this config for values for "k".
        ++ [("M-s " ++ k, S.promptSearch dtXPConfig' f) | (k,f) <- searchList ]
        -- [("M-S-s " ++ k, S.selectSearch f) | (k,f) <- searchList ]

    -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

------------------------------------------------------------------------
main :: IO ()
main = do

    -- Launch xmobar
    xmproc <- spawnPipe "xmobar $HOME/.config/xmobar/xmobar.conf"

    xmonad $ docks $ ewmh def
        { manageHook         = myManageHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormColor
        , focusedBorderColor = myFocusColor
        , logHook = historyHook <+> workspaceHistoryHook <+> myLogHook <+> setWMName "LG3D" <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x
                        , ppCurrent = xmobarColor "#46D9FF" "" . wrap
                                      ("<box type=Bottom width=2 mb=2 color=" ++ "#46D9FF" ++ ">") "</box>"           -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#51afef" ""                          -- Visible but not current workspace
                        , ppHidden = xmobarColor "#99bb66" "" . wrap
                                     ("<box type=Top width=2 mt=2 color=" ++ "#99bb66" ++ ">") "</box>"                 -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#99bb66" ""                  -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#51afef" "" . shorten 60               -- Title of active window in xmobar
                        , ppSep =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
                        , ppExtras  = [windowCount]                                     -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
        } `additionalKeysP` myKeys
